# TASK MANAGER

## DEVELOPER INFO

name: Ruslan Pavlov

email: o000.ruslan@yandex.ru

email: rpavlov@tsconsulting.com

## HARDWARE

CPU: i5

RAM: 16GB

SSD: 512GB

## SOFTWARE

System: Windows 10

Version JDK: 1.8.0_281

## PROGRAM RUN
```
java -jar task-manager.jar
```
## TASK SCREENSHOTS

https://drive.google.com/drive/folders/1WXWPbp-s2XSKkye-1V2RDbhXjw1cQ00j